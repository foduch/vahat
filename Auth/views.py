from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.template.context_processors import csrf
from django.template import RequestContext


def home(request):
    args ={}
    args["username"]=auth.get_user(request).username
    return render_to_response('base.html', args)

def login(request):
    args={}
    args.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            args['login_error'] = 'error'
            return render_to_response('login.html', args, context_instance=RequestContext(request))
    else:
        return render_to_response('Login.html', args, context_instance=RequestContext(request))


def logout(request):
    auth.logout(request)
    return redirect('/')