from django.conf.urls import include, url
from Auth.views import home, login, logout

urlpatterns = [
    url(r'^$', home, name="home"),
    url(r'login/', login, name="login"),
    url(r'logout/', logout, name='logout'),
    #url(r'signup/', 'Auth.views.signup', name='signup'),

]
