from django import forms
from Athlete.models import Athlete


class SomeForm(forms.Form):
    text = forms.CharField(max_length=30)


class NewAthlete(forms.Form):
    first_name = forms.CharField(max_length=50, label="Имя")
    last_name = forms.CharField(max_length=50, label="Фамилия")
    birthday = forms.DateField(input_formats=('%d.%m.%Y',),label="День рождения")
    sex = forms.ChoiceField(choices=(("М", "Мужской"), ("Ж", "Женский")), label="Пол")
    region = forms.CharField(max_length=50, label="Регион", required=False)
    team = forms.CharField(max_length=50, label="Команда", required=False)
    degree = forms.ChoiceField(choices=(("МСМК", "МСМК"), ("МС", "МС"), ("КМС", "КМС"),
                                       ( "1", "I"), ("2", "II"), ("3", "III"),
                                       ("1ю", "Iю"), ("2ю", "IIю"), ("3ю", "IIIю"), ("б/р", "б/р"))
                               , label="Разряд")


    def save(self):
        Athlete(**self.cleaned_data).save()
