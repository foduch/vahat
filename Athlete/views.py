from django.http import HttpResponseRedirect
from django.shortcuts import render
from Athlete.models import *
from Competitions.models import *
from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.template.context_processors import csrf
from django.template import RequestContext
from Athlete.forms import NewAthlete as NewAthl, SomeForm


# Create your views here.
def TableOfAthletes(request):
    args = {'Athlete': Athlete.objects.filter()}
    args["username"] = auth.get_user(request).username
    return render(request, 'TableOfAthlethes.html', args)

def NewAthlete(request):
    args = {}
    args["username"] = auth.get_user(request).username
    if request.method == 'POST':
        form = NewAthl(request.POST)
        args['form'] = form
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/athletes/')
        else:
            return render(request, 'NewAthlete.html', args)
    else:
        args['form'] = NewAthl()
        return render(request, 'NewAthlete.html', args)

def ATH(request, id):
    args = {}
    args["athlete"] = Athlete.objects.get(id=id)
    args["username"] = auth.get_user(request).username
    part = Participant.objects.filter(athlete_id=id).values("race_id", "race__name")
    args["part"] = part
    return render(request, 'Athlete.html', args)