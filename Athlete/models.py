from django.db import models

# Create your models here.

'''class Coach(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    first_name_lat = models.CharField(max_length=50)
    last_name_lat = models.CharField(max_length=50)
    birthday = models.DateField()
    sex = models.CharField(max_length=50, choices=(("M", "Male"),
                                                   ("F", "Female")))

    def __str__(self):
        return self.first_name+ ' '+ self.last_name'''

class Athlete(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birthday = models.DateField(format('%d.%m.%Y'))
    sex = models.CharField(max_length=50, choices=(("М", "Мужской"), ("Ж", "Женский")))
    region = models.CharField(max_length=50, blank=True)
    team = models.CharField(max_length=50, blank=True)
    degree = models.CharField(max_length=50,
                              choices=(("МСМК", "МСМК"), ("МС", "МС"), ("КМС", "КМС"),
                                       ( "1", "I"), ("2", "II"), ("3", "III"),
                                       ("1ю", "Iю"), ("2ю", "IIю"), ("3ю", "IIIю"), ("б/р", "б/р")))

    def __str__(self):
        return self.first_name+ ' '+ self.last_name




