# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Athlete', '0004_auto_20171216_0856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='athlete',
            name='degree',
            field=models.CharField(max_length=50, choices=[('МСМК', 'МСМК'), ('МС', 'МС'), ('КМС', 'КМС'), ('1', 'I'), ('2', 'II'), ('3', 'III'), ('1ю', 'Iю'), ('2ю', 'IIю'), ('3ю', 'IIIю'), ('б/р', 'б/р')]),
        ),
    ]
