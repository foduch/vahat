# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Athlete', '0003_auto_20171216_0825'),
    ]

    operations = [
        migrations.AlterField(
            model_name='athlete',
            name='birthday',
            field=models.DateField(verbose_name='%d.%m.%Y'),
        ),
        migrations.AlterField(
            model_name='athlete',
            name='degree',
            field=models.CharField(max_length=50, choices=[('МСМК', 'МСМК'), ('МС', 'МС'), ('КМС', 'КМС'), ('I', '1'), ('2', 'II'), ('III', '3'), ('I ю', '1ю'), ('II ю', '2ю'), ('III ю', '3ю'), ('б/р', ' ')]),
        ),
        migrations.AlterField(
            model_name='athlete',
            name='sex',
            field=models.CharField(max_length=50, choices=[('М', 'Мужской'), ('Ж', 'Женский')]),
        ),
    ]
