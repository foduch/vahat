# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Athlete',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birthday', models.DateField()),
                ('sex', models.CharField(choices=[('Мужской', 'М'), ('Женский', 'Ж')], max_length=50)),
                ('region', models.CharField(blank=True, max_length=50)),
                ('team', models.CharField(max_length=50)),
                ('degree', models.CharField(choices=[('МСМК', 'МСМК'), ('МС', 'МС'), ('КМС', 'КМС'), ('I', '1'), ('II', '2'), ('III', '3'), ('I ю', '1ю'), ('II ю', '2ю'), ('III ю', '3ю'), ('б/р', ' ')], max_length=50)),
            ],
        ),
    ]
