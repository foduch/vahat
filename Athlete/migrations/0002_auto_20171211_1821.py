# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Athlete', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='athlete',
            name='team',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
