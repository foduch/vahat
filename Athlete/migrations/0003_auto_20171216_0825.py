# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Athlete', '0002_auto_20171211_1821'),
    ]

    operations = [
        migrations.AlterField(
            model_name='athlete',
            name='birthday',
            field=models.DateField(verbose_name='%d%m%Y'),
        ),
    ]
