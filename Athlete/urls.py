from django.conf.urls import include, url
from Athlete.views import TableOfAthletes, NewAthlete, ATH

urlpatterns = [
    url(r'^$', TableOfAthletes, name="TableOfAthletes"),
    url(r'new', NewAthlete, name="NewAthlete"),
    url(r'(?P<id>\d+)/$', ATH, name="ATH"),


]