from django.conf.urls import include, url
from API.views import GetAthletes, GetCategories, GetParticipants, SetPoint, StartRace, Test, ListOfApi

urlpatterns = [
    url(r'get_athletes', GetAthletes, name='GetAthletes'),
    url(r'get_categories', GetCategories, name='GetCategories'),
    url(r'get_participants', GetParticipants, name="GetParticipants"),
    url(r'set_point', SetPoint, name='SetPoint'),
    url(r'start_race', StartRace, name='StartRace'),
    url(r'test', Test, name='Test'),
    url(r'$', ListOfApi, name="ListOfApi")

]