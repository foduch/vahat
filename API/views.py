from django.shortcuts import render
from Athlete.models import Athlete
from Competitions.models import *
from django.http import JsonResponse
import json
from datetime import datetime

# Create your views here.
def GetAthletes(request):
    if "id" in request.GET.keys():
        try:
            id=int(request.GET["id"])
            athl=Athlete.objects.filter(id=request.GET["id"]).values('id', 'first_name', 'last_name')
        except:
            err="invalid athlete ID"
            return JsonResponse({"error":err})
        return JsonResponse({"res": list(athl)})
    else:
        athl= Athlete.objects.filter().values('id', 'first_name', 'last_name')
        return JsonResponse({"res": list(athl)})

def GetCategories(request):
    cat = Category.objects.filter().values('id', 'name')
    return JsonResponse({"res": list(cat)})

def GetParticipants(request):
    if "race_id" in request.GET.keys():
        try:
            race_id = int(request.GET["race_id"])
            Competition.objects.get(id=race_id)
        except:
            err = "invalid race id"
            return JsonResponse({"error": err})

        part = Participant.objects.filter(
            race_id=request.GET["race_id"]).values("number", "athlete_id",
                               "athlete__first_name", "athlete__last_name", "category__name",
                               "category_id")
        return JsonResponse({"res": list(part)})
    if "athlete_id" in request.GET.keys():
        try:
            athlete_id = int(request.GET["athlete_id"])
            Athlete.objects.get(id=athlete_id)
        except:
            err = "invalid race id"
            return JsonResponse({"error": err})

        part = Participant.objects.filter(
            athlete_id=request.GET["athlete_id"]).values("number", "athlete__first_name",
                                                      "athlete__last_name", "race__name", "race_id")
        return JsonResponse({"res": list(part)})
    part=Participant.objects.filter().values("number", "athlete_id", "race_id", "race__name")
    return JsonResponse({"res":list(part)})

def SetPoint(request):
    if "race_id" in request.GET.keys():
        if "num" in request.GET.keys():
            if "dtime" in request.GET.keys():
                try:
                    num = int(request.GET["num"])
                except:
                    err="not a number"
                    return JsonResponse({"error": err})
                try:
                    race_id=int(request.GET["race_id"])
                    Competition.objects.get(id=race_id)
                except:
                    err="invalid race id"
                    return JsonResponse({"error": err})
                try:
                    dt=datetime.strptime(request.GET["dtime"], '%Y-%m-%d %H:%M:%S')
                except:
                    err="wrong datetime format"
                    return JsonResponse({"error": err})
                p = Point(race_id=race_id, number=num,
                          dt=dt)
                p.save()
                return JsonResponse({"res": "ok"})
            else:
                return JsonResponse({"error": "not datetime info"})
        else:
            return JsonResponse({"error": "not racer number info"})
    else:
        return JsonResponse({"error": "not race id info"})

def StartRace(request):
    if "race_id" in request.GET.keys():
        try:
            race_id = int(request.GET["race_id"])
            Competition.objects.get(id=race_id)
        except:
            err = "invalid race id"
            return JsonResponse({"error": err})
    else:
        return JsonResponse({"error": "not race id info"})
    if "dtime" in request.GET.keys():
        try:
            dt = datetime.strptime(request.GET["dtime"], '%Y-%m-%d %H:%M:%S')
        except:
            err = "wrong datetime format"
            return JsonResponse({"error": err})
    else:
        return JsonResponse({"error": "not datetime info"})
    part=Participant.objects.filter(race_id=race_id).values("number")
    for athl in part:
        try:
            Point.objects.get(race_id=race_id, number=athl["number"],
                dt=dt)
        except:
            p = Point(race_id=race_id, number=athl["number"],
                  dt=dt)
            p.save()
    return JsonResponse({"res": "ok"})

def Test(request):
    race_id = int(request.GET["race_id"])
    r = dict()
    z = Participant.objects.filter(race_id=race_id).values("category")
    zz = set()
    for item in z:
        zz.update({item["category"]})

    for cat in zz:

        part = Participant.objects.filter(race_id=race_id, category_id=cat).values("number", "athlete")
        res=[]
        for item in part:
            a = {}
            p = Point.objects.filter(number=item["number"], race_id=race_id).values("dt").order_by("dt")
            a["laps"]=list(p)
            a["result"] = str(a["laps"][-1]["dt"] - a["laps"][0]["dt"])
            laps=[]
            for i in range(1, len(a["laps"])):
               laps.append(str(a["laps"][i]["dt"]-a["laps"][i-1]["dt"]))
            a["laps"]=laps

            a["athlete"]=str(Athlete.objects.get(id=item["athlete"]))
            a["nlaps"]=len(a["laps"])

            res.append(a)

        res = sorted(res, key=lambda x: (x["nlaps"], x["result"]))
        r[str(Category.objects.get(id=cat))]=res


    return JsonResponse({"res": r})

def ListOfApi(request):
    return render(request, 'APIList.html')