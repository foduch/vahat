from django.shortcuts import render
from django.shortcuts import render_to_response
from django.contrib import auth
# Create your views here.

def homepage(request):
    args = {}
    args["username"] = auth.get_user(request).username
    return render_to_response('base.html', args)