from django import forms
from Competitions.models import Category, Competition, Participant
from Athlete.models import Athlete


class NewCategory(forms.Form):
    name = forms.CharField(max_length=50, label='Название')
    min_age = forms.IntegerField(label='Минимальный возраст')
    max_age = forms.IntegerField(label='Максимальный возраст')
    sex = forms.ChoiceField(choices=(("М", "Мужской"), ("Ж", "Женский")), label="Пол")


    def save(self):
        Category(**self.cleaned_data).save()

class NewParticipant(forms.Form):
    athlete = forms.ModelMultipleChoiceField(queryset=Athlete.objects.all())
    category = forms.ModelMultipleChoiceField(queryset=Category.objects.all())

    def save(self):
        Participant(**self.cleaned_data).save()