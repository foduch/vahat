from django.db import models
from Athlete.models import *
from django.utils import timezone
# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=50)
    min_age = models.IntegerField()
    max_age = models.IntegerField()
    sex = models.CharField(max_length=50, choices=(("М", "Мужской"), ("Ж", "Женский")))

    def __str__(self):
        return(self.name)

class Competition(models.Model):
    name = models.CharField(max_length=150)
    date = models.DateField()
    type = models.CharField(max_length=50)
    place = models.CharField(max_length=150)

    def __str__(self):
        return(self.name)

class Participant(models.Model):
    athlete = models.ForeignKey(Athlete)
    race = models.ForeignKey(Competition)
    category = models.ForeignKey(Category)
    number = models.IntegerField(blank=True)

    def __str__(self):
        return self.race.name+' '+self.category.name+' '+str(self.athlete)

class Point(models.Model):
    number = models.IntegerField()
    race = models.ForeignKey(Competition)
    dt = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.race.name+' '+str(self.number)+' '+str(self.dt)
