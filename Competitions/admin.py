from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Category)
admin.site.register(Competition)
admin.site.register(Participant)
admin.site.register(Point)