from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.template.context_processors import csrf
from django.template import RequestContext
from Competitions.forms import NewCategory as NewCat, NewParticipant as NewPart
from Competitions.models import *
from django.db import connection
from django.http import Http404
from datetime import datetime

# Create your views here.
def Comp(request):
    args = {}
    args["comp"] = Competition.objects.filter()
    return render(request, "TableOfCompetitions.html", args)

def NewCategory(request):
    args = {}
    if request.method == 'POST':
        form = NewCat(request.POST)
        args['form'] = form
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/competitions/')
        else:
            return render(request, 'NewCategory.html', args)
    else:
        args['form'] = NewCat()
        return render(request, 'NewCategory.html', args)

def NewParticipant(request, id):
    args = {}
    if request.method == 'POST':
        form = NewPart(request.POST)
        args['form'] = form
        if len(Participant.objects.filter(race_id=id, athlete_id=request.POST['athlete'])) > 0:
            args["error"]='Этот участник уже зарегестрирован'
            return render(request, 'NewParticipant.html', args)
        if form.is_valid():
            Participant(athlete_id=request.POST['athlete'],
                        category_id=request.POST['category'], race_id=id, number=0).save()
            return HttpResponseRedirect('/competitions/'+str(id))
        else:
            return render(request, 'NewParticipant.html', args)
    else:
        args['form'] = NewPart()
        args['act'] = '/competitions/'+str(id)+'/new_part'
        return render(request, 'NewParticipant.html', args)

def VCompetition(request, id):
    try:
        Competition.objects.get(id=id)
    except:
        raise Http404
    race_id = id
    r = {}
    z = Participant.objects.filter(race_id=race_id).values("category")
    zz = set()
    for item in z:
        zz.update({item["category"]})

    args = {}
    comp = Competition.objects.get(id=id)
    args["comp"] = comp
    args["part"] = Participant.objects.filter(race=comp)
    if len(Point.objects.filter(race_id=id)) == 0:
        for cat in zz:
            part = Participant.objects.filter(race_id=race_id, category_id=cat).values("number", "athlete_id")
            c = str(Category.objects.get(id=cat))
            a=[]
            for item in part:
                p={}
                p["athlete"]=str(Athlete.objects.get(id=item["athlete_id"]))
                p["number"]=item["number"]
                p["athlete_id"]=item["athlete_id"]
                a.append(p)
            r[c] = a
        args["part"]=r
        return render(request, 'Competition.html', args)

    for cat in zz:

        part = Participant.objects.filter(race_id=race_id, category_id=cat).values("number", "athlete", "athlete_id")
        res = []
        for item in part:
            a = {}
            p = Point.objects.filter(number=item["number"], race_id=race_id).values("dt").order_by("dt")
            a["laps"] = list(p)
            a["athlete_id"] = str(item["athlete_id"])
            a["result"] = str(a["laps"][-1]["dt"] - a["laps"][0]["dt"])
            laps = []
            for i in range(1, len(a["laps"])):
                laps.append(str(a["laps"][i]["dt"] - a["laps"][i - 1]["dt"]))
            a["laps"] = laps

            a["athlete"] = str(Athlete.objects.get(id=item["athlete"]))
            a["nlaps"] = len(a["laps"])

            res.append(a)

        res = sorted(res, key=lambda x: (x["nlaps"], x["result"]))
        for i in range(1, len(res)+1):
            res[i-1]["place"]=i
        c = str(Category.objects.get(id=cat))

        FirstResult=datetime.strptime(res[0]["result"], '%H:%M:%S')
        for athlete in res:
            athlete["gap"]=str(datetime.strptime(athlete["result"], '%H:%M:%S')
                               -FirstResult)
        r[c] = res

    args["res"] = r
    return render(request, 'Competition.html', args)

