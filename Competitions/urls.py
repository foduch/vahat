from django.conf.urls import include, url
from Competitions.views import Comp, NewCategory, VCompetition, NewParticipant

urlpatterns = [
    url(r'^$', Comp, name="Comp"),
    url(r'newcategory', NewCategory, name="NewCategory"),
    url(r'(?P<id>\d+)/$', VCompetition, name="VCompetition"),
    url(r'(?P<id>\d+)/new_part', NewParticipant, name='NewParticipant')

]